Proyecto 2 Unidad 2: Carrera

En la primera version de este proyecto hubo muchos errores de cálculo en las estadísticas de las partes de las Naves por el hecho de que no habían restricciones para 
los daños que estas podían sufrir. Se tuvo que poner ojo en esto en las siguientes versiones del juego, ya que, uno no está acostumbrado a delimitar las leyes de la física
en el mundo real.

En la siguiente versión se delimitó por medio de funciones el daño que podían sufrir las naves y los factores (parámetros) que tienen influencia sobre éste. Pero aún quedaba 
el problema de un ciclo sin orden: el tiempo pasaba de manera diferente para cada nave dependiendo de cuál era su acción durante la carrera: una podía pasar entre 8 y 15 
segundos acelerando de manera constante mientras que otra, durante la misma iteración del ciclo, podía pasar 10 minutos a velocidad constante perdiendo todas sus estadísticas 
rápidamente.

Luego nos enfrentamos a la duración de la carrera en forma virtual y su paralelo en la realidad, debido a que esta carrera está hecha para durar un máximo de 2 horas o tener 
un finalista, se ideó una manera de hacer que el tiempo mínimo necesario en la carrera fuera el de la duración del ciclo, es decir: 15 segundos (virtuales) en 500 ticks 
(medio segundo real) dando lugar a un código que correrá por, como máximo, 4 minutos. Esto fue ideado para que las naves tuvieran un lapso de 15 segundos para acelerar, 
avanzar a velocidad constante, reducir su velocidad o reparar cierto porcentaje de sus estadísticas una vez por cada ciclo.

De esta manera serían resueltos los problemas de las versiones anteriores definiendo las acciones que las naves harían por medio de un ciclo secundario al ciclo de juego al 
inicio de éste, luego realizarían su acción previamente definida en un segundo ciclo secundario al juego y finalmente serían anunciadas sus acciones y consecuencias por el
Locutor en un penúltimo ciclo secundario.

Para finalizar el juego se necesitarían 2 condiciones: tener un finalista o que se cumplan las 2 horas de carrera. Dependiendo de una u otra se imprimiría el nombre del único 
ganador, seguido de los lugares en que quedaron el resto de los participantes o se imprimirían todos como finalistas en los lugares en que quedaron al acabar el tiempo, con lo 
cual el ganador sería el que terminó más lejos; de cualquiera de ambas maneras, el juego acaba al cambiar el valor del booleano del ciclo principal por un false.