
public class Ala {
	
	private float resistenciaA;
	private float resistencia_actual;
	private float aerodinamica;
	private float aerodinamica_actual;
	
	
	Ala(){}
	
	
	public float actualizador_resistencia_a_velocidad_constante(float aerodinamica_actual, float velocidad_actual, float resistencia_Actual, float resistencia_total, float capacidad_maxima_de_velocidad) {
		
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades);
		float distancia_en_metros = (velocidad_en_metros_por_segundo * 15f), distancia_en_km = (distancia_en_metros / 1000f);
		float degradacion_base = (resistencia_total / 6000f), indice_de_intencidad = (velocidad_actual / capacidad_maxima_de_velocidad);
		float degradacion_neta_por_km = degradacion_base * indice_de_intencidad, degradacion_total = (degradacion_neta_por_km * distancia_en_km);
		
		float variable_intermedia = (aerodinamica_actual /1300f), variable_intermedia2 = (1300f + aerodinamica_actual), variable_intermedia3 = (1300f / variable_intermedia2);
		float mitigacion_de_daño = (variable_intermedia3 * variable_intermedia);
	
		float porcentaje_daño = (1f - mitigacion_de_daño);
		
		float daño = (degradacion_total * porcentaje_daño);
		
		float resistencia_nueva = (resistencia_actual - daño);
		
		return resistencia_nueva;
	}
	
	public float actualizador_resistencia_acelerando(float aerodinamica_actual, float velocidad_actual, float variacion_efectiva, float resistencia_Actual, float resistencia_total, float capacidad_maxima_de_velocidad) {
		
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades); 
		float variacion_en_metros_por_segundo = (variacion_efectiva * transformador_de_velocidades), velocidad_media = ((velocidad_en_metros_por_segundo + variacion_en_metros_por_segundo)/2f), distancia_en_metros = (velocidad_media * 15f);
		float velocidad_media_en_km_por_hora = (velocidad_media / transformador_de_velocidades), distancia_en_km = (distancia_en_metros / 1000f);
		float degradacion_base = (resistencia_total / 6000f), indice_de_intencidad = (velocidad_media_en_km_por_hora / capacidad_maxima_de_velocidad);
		float degradacion_neta_por_km = degradacion_base * indice_de_intencidad, degradacion_total = (degradacion_neta_por_km * distancia_en_km);
		
		float variable_intermedia = (aerodinamica_actual /1300f), variable_intermedia2 = (1300f + aerodinamica_actual), variable_intermedia3 = (1300f / variable_intermedia2);
		float mitigacion_de_daño = (variable_intermedia3 * variable_intermedia);
	
		float porcentaje_daño = (1f - mitigacion_de_daño);
		
		float daño = (degradacion_total * porcentaje_daño);
		
		float resistencia_nueva = (resistencia_actual - daño);
		
		return resistencia_nueva;
	}
	
	
	public float actualizador_aerodinamica_a_vel_constante(float aerodinamica_actual, float aerodinamica, float velocidad_actual, float resistencia, float resistencia_actual, float capacidad_maxima_de_velocidad) {
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades);
		float distancia_en_metros = (velocidad_en_metros_por_segundo * 15f), degradacion_base = (aerodinamica / 8000f), indice_de_intencidad = (velocidad_actual / capacidad_maxima_de_velocidad);
		float daño_actual = (resistencia - resistencia_actual), distancia_en_km = (distancia_en_metros / 1000f);
		
		double indice_de_daño_recibido = (Math.log(daño_actual));
		
		double degradacion_efectiva = (degradacion_base * indice_de_intencidad * indice_de_daño_recibido);
		
		float degradacion_efectivaf = Math.round(degradacion_efectiva), aerodinamica_nueva = (degradacion_efectivaf * distancia_en_km);
		
		return aerodinamica_nueva;
	}
	
	
	public float actualizador_aerodinamica_acelerando(float aerodinamica_actual, float aerodinamica, float velocidad_actual, float variacion_velocidad, float resistencia, float resistencia_actual, float capacidad_maxima_de_velocidad) {
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades), variacion_en_metros_por_segundo = (variacion_velocidad * transformador_de_velocidades);
		float degradacion_base = (aerodinamica / 8000f), velocidad_media = ((velocidad_en_metros_por_segundo + variacion_en_metros_por_segundo)/2f), distancia_en_metros = (velocidad_en_metros_por_segundo * 15f),  indice_de_intencidad = (velocidad_media / capacidad_maxima_de_velocidad);
		float daño_actual = (resistencia - resistencia_actual), distancia_en_km = (distancia_en_metros / 1000f);
		
		double indice_de_daño_recibido = (Math.log(daño_actual));
		
		double degradacion_efectiva = (degradacion_base * indice_de_intencidad * indice_de_daño_recibido);
		
		float degradacion_efectivaf = Math.round(degradacion_efectiva), aerodinamica_nueva = (degradacion_efectivaf * distancia_en_km);
		
		return aerodinamica_nueva;
	}

	public float aerodinamica_recuperada(float resistencia_total, float resistencia_nueva, float aerodinamica_total) {
		
		float porcentuador = (100f / resistencia_total);
		float porcentaje_nuevo_de_resistencia = (resistencia_nueva * porcentuador), porcentaje_nuevo_de_aerodinamica = (porcentaje_nuevo_de_resistencia * aerodinamica_total);
		
		float aerodinamica_nueva = (porcentaje_nuevo_de_aerodinamica / 100f);
		
		return aerodinamica_nueva;
	}

	

	public float getResistenciaA() {
		return resistenciaA;
	}
	public void setResistenciaA(float resistenciaA) {
		this.resistenciaA = resistenciaA;
	}
	

	public float getAerodinamica() {
		return aerodinamica;
	}
	public void setAerodinamica(float aerodinamica) {
		this.aerodinamica = aerodinamica;
	}


	public float getResistencia_actual() {
		return resistencia_actual;
	}
	public void setResistencia_actual(float resistencia_actual) {
		this.resistencia_actual = resistencia_actual;
	}


	public float getAerodinamica_actual() {
		return aerodinamica_actual;
	}
	public void setAerodinamica_actual(float aerodinamica_actual) {
		this.aerodinamica_actual = aerodinamica_actual;
	}

}
