
public class Motor {
	
	private float resistenciaM;
	private float resistencia_actual;

	private float capacidad_de_aceleracion;
	private float capacidad_maxima_de_velocidad_del_motor;
	
	Motor(){}
	
	public float actualizador_resistencia_acelerando(float velocidad_actual, float aceleracion, float resistencia_actual, float resistencia_total, float capacidad_maxima_de_velocidad) {
		float transformador_kmporhora_a_mporsegundo = (1000f / 3600f), v_a = (velocidad_actual * transformador_kmporhora_a_mporsegundo);
		float ac = (aceleracion * transformador_kmporhora_a_mporsegundo);
		float velocidad_media = ((v_a + ac)/2f), distancia_recorrida = (velocidad_media * 15f), distancia_recorrida_en_km = (distancia_recorrida / 1000f);
		
		float velocidad_media_en_km_por_hora = (velocidad_media / transformador_kmporhora_a_mporsegundo), degradacion_base = (resistencia_total / 6000f);
		float indice_de_intencidad = (velocidad_media_en_km_por_hora / capacidad_maxima_de_velocidad), daño_neto_por_km = (degradacion_base * indice_de_intencidad);
		
		float daño = (daño_neto_por_km * distancia_recorrida_en_km);
		
		float resistencia_nueva = (resistencia_actual - daño);
		
		return resistencia_nueva;
	}
	
	
	
	public float actualizador_resistencia_a_velocidad_constante(float velocidad_actual, float resistencia_Actual, float resistencia_total, float capacidad_maxima_de_velocidad) {
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades);
		float distancia_en_metros = (velocidad_en_metros_por_segundo * 15f), distancia_en_km = (distancia_en_metros / 1000f);
				
		float degradacion_base = (resistencia_total / 6000f), indice_de_intencidad = (velocidad_actual / capacidad_maxima_de_velocidad), daño_neto_por_km = (degradacion_base * indice_de_intencidad);
		
		float daño = (daño_neto_por_km * distancia_en_km);
		
		float resistencia_nueva = (resistencia_actual - daño);
		
		return resistencia_nueva;
	}
	
	public float capacidad_de_aceleracion_en_funcion_de_la_velocidad_actual(float capacidad_de_aceleracion, float velocidad_actual, float capacidad_maxima_de_velocidad) {
		float esfuerzo_imposible = (velocidad_actual / capacidad_maxima_de_velocidad),esfuerzo_posible = (1f - esfuerzo_imposible), capacidad_de_aceleracion_actual = (capacidad_de_aceleracion * esfuerzo_posible);
		
		return capacidad_de_aceleracion_actual;
	}

	

	//Getter y Setter de resistencia.
	public float getResistenciaM() {
		return resistenciaM;
	}
	public void setResistenciaM(float resistenciaM) {
		this.resistenciaM = resistenciaM;
	}
	//Getter y Setter de resistencia actual.
	public float getResistencia_actual() {
		return resistencia_actual;
	}
	public void setResistencia_actual(float resistencia_actual) {
		this.resistencia_actual = resistencia_actual;
	}



	public float getCapacidad_de_aceleracion() {
		return capacidad_de_aceleracion;
	}

	public void setCapacidad_de_aceleracion(float capacidad_de_aceleracion) {
		this.capacidad_de_aceleracion = capacidad_de_aceleracion;
	}

	public float getCapacidad_maxima_de_velocidad_del_motor() {
		return capacidad_maxima_de_velocidad_del_motor;
	}

	public void setCapacidad_maxima_de_velocidad_del_motor(float capacidad_maxima_de_velocidad_del_motor) {
		this.capacidad_maxima_de_velocidad_del_motor = capacidad_maxima_de_velocidad_del_motor;
	}
	
	
	
	
	
}
