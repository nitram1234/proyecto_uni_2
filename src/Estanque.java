import java.util.Random;
public class Estanque {
	
	private int capacidad;
	private float cantidad_actual;
	private Combustible combustible;	
	
	private int number;
	int number2;
	
	Estanque(){
		this.combustible = new Combustible();	
	}
	
	
	public void definir_combustible() {
		
		if (number == 0 ) {
			Random numero2 = new Random();
			number2 = numero2.nextInt(3);
			
			combustible.setNombre("Nuclear");
			combustible.setQuemaje(1);
			combustible.setIndice_aceleracion(1.04f);
			
			if (number2 == 0) {
				capacidad = 4500;
				cantidad_actual = 4500;
				
			}
			if (number2 == 1) {
				capacidad = 5000;
				cantidad_actual = 5000;
			}
			if (number2 == 2) {
				capacidad = 5500;
				cantidad_actual = 5500;
			}			
		}
		
		if (number == 1) {			
			Random numero2 = new Random();
			number2 = numero2.nextInt(3);
			
			combustible.setNombre("Diesel");
			combustible.setQuemaje(7);
			combustible.setIndice_aceleracion(1.02f);
			
			if (number2 == 0) {
				capacidad = 34000;
				cantidad_actual = 34000;
			}
			if (number2 == 1) {
				capacidad = 35000;
				cantidad_actual = 35000;
			}
			if (number2 == 2) {
				capacidad = 36000;
				cantidad_actual = 36000;
			}			
		}
		
		if (number == 2) {			
			Random numero2 = new Random();
			number2 = numero2.nextInt(3);
			
			combustible.setNombre("Biodiesel");
			combustible.setQuemaje(10);
			combustible.setIndice_aceleracion(1.003f);
			
			if (number2 == 0) {
				capacidad = 48000;
				cantidad_actual = 48000;
			}
			if (number2 == 1) {
				capacidad = 50000;
				cantidad_actual = 50000;
			}
			if (number2 == 2) {
				capacidad = 52000;
				cantidad_actual = 52000;
			}
		}
	}
	
		
	public int actualizador_combustible_a_velocidad_constante(float quemaje, float combustible_actual, float velocidad_actual) {
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades);
		float distancia = (velocidad_en_metros_por_segundo * 15f), distancia_en_km = (distancia / 1000f);//la variable distancia son los km recorridos desde la ultima actualizacion.

		float indice_de_intencidad = (velocidad_actual/2800f);
		float quemaje_segun_velocidad = (quemaje * indice_de_intencidad), combustible_quemado = (distancia_en_km * quemaje_segun_velocidad);//cq es el combustible quemado desde la ultima actualizacion.
		
		int combustible_nuevo = Math.round(combustible_actual-combustible_quemado);
		
		return combustible_nuevo;
	}
	
	
	
	public int actualizador_combustible_acelerando(float quemaje, float combustible_actual, float velocidad_actual, float variacion_efectiva) {
		
		float transformador_de_velocidades = (1000f / 3600f), velocidad_en_metros_por_segundo = (velocidad_actual * transformador_de_velocidades), variacion_en_m_por_s = (variacion_efectiva * transformador_de_velocidades);
		float velocidad_media = ((velocidad_en_metros_por_segundo + variacion_en_m_por_s) / 2f), distancia = (velocidad_media * 15f), distancia_en_km = (distancia / 1000f);//la variable distancia son los km recorridos desde la ultima actualizacion.

		float velocidad_media_en_km_por_hora = (velocidad_media / transformador_de_velocidades), indice_de_intencidad = (velocidad_media_en_km_por_hora/2800f);
		float quemaje_segun_velocidad = (quemaje * indice_de_intencidad), combustible_quemado = (distancia_en_km * quemaje_segun_velocidad);//cq es el combustible quemado desde la ultima actualizacion.
		
		int combustible_nuevo = Math.round(combustible_actual-combustible_quemado);
		
		return combustible_nuevo;
	}
	
		
	public float getCantidad_actual() {
		return cantidad_actual;
	}
	public void setCantidad_actual(float cantidad_actual) {
		this.cantidad_actual = cantidad_actual;
	}
	

	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}	

	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}


	public Combustible getCombustible() {
		return combustible;
	}
	 
	
}
