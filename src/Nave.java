
public interface Nave {

	
	Boolean getAndando();

	void setAndando(Boolean andando);
	
	
	String getEstado();
	void setEstado(String estado);
	
	int getDelanteros();
	void setDelanteros(int delanteros);
	
	
	Ala getDerecha();

	void setDerecha(Ala derecha);

	Ala getIzquierda();

	void setIzquierda(Ala izquierda);

	Motor getMotor();

	void setMotor(Motor motor);

	Estanque getEstanque();

	void setEstanque(Estanque estanque);

	Odometro getOdometro();

	void setOdometro(Odometro odometro);

	String getNombre();

	void setNombre(String nombre);

	void definir_motor();

	void definir_alas();

	void definir_odometro();

	void definir_estanque();


}