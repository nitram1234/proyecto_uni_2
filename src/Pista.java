import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class Pista {
	
	private int largo = 6000;	
	private ArrayList<Nave> naves = new ArrayList<Nave>();
	private ArrayList<Reparador> reparadores = new ArrayList<Reparador>();
	private int cantidad, tipo_de_nave;
	//private String nombre_jugador;
	
	
	Scanner sc = new Scanner(System.in);
	
	
	Pista(int cantidad){
		for (int i = 0; i < (cantidad); i++) {
			
			//if (i == 0) {}
			
			Random random = new Random();
			tipo_de_nave = random.nextInt(3);
			
			if (tipo_de_nave == 0 ) {
				String name ="bot_nuclear "+i;
				Nave bot_nuclear = new Nave_nuclear(name);
				naves.add(bot_nuclear);
			}
			
			if (tipo_de_nave == 1 ) {
				String name ="bot_diesel "+i;
				Nave bot_diesel = new Nave_diesel(name);
				naves.add(bot_diesel);
			}
			
			if (tipo_de_nave == 2 ) {
				String name ="bot_biodiesel"+i;
				Nave bot_biodiesel = new Nave_biodiesel(name);
				naves.add(bot_biodiesel);
			}
						
		}
		
		
		for (int j = 0; j < (largo / 3000); j++) {
			
			Reparador r = new Reparador();
			reparadores.add(r);
			
		}
	}
	
	public void ubicador_reparadores(int largo) {
		
	}

	public ArrayList<Nave> getNaves() {
		return naves;
	}
	public void setNaves(ArrayList<Nave> naves) {
		this.naves = naves;
	}
	




	public ArrayList<Reparador> getReparadores() {
		return reparadores;
	}
	public void setReparadores(ArrayList<Reparador> reparadores) {
		this.reparadores = reparadores;
	}


	public int getLargo() {
		return largo;
	}
	public void setLargo(int largo) {
		this.largo = largo;
	}
	
	
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
	



